#! /usr/bin/python

##################################################################
# Project: Python Code Formatter
# Filename: ui_codeformatter.py
# Description: User interface for source code formatting
# Date: 2009.05.24
# Programmer: Lin Xin Yu
# E-mail: nxforce@yahoo.com
# Website:http://importcode.blogspot.com
##################################################################

#================= import modules ================================

from PyQt4 import QtCore, QtGui

#================= Ui_CodeFormatterClass Start ===================
	
class Ui_CodeFormatterClass(object):
	def setupUi(self, CodeFormatterClass):
		CodeFormatterClass.setObjectName("CodeFormatterClass")
		CodeFormatterClass.resize(800, 630)
		
		# plain text editor
		self.textEdit_raw = QtGui.QTextEdit(CodeFormatterClass)
		self.textEdit_raw.setGeometry(QtCore.QRect(10, 10, 780, 285))
		self.textEdit_raw.setObjectName("textEdit_raw")
		self.textEdit_raw.setTabStopWidth(20)
		
		# formatted text editor
		self.textEdit_fmt = QtGui.QTextEdit(CodeFormatterClass)
		self.textEdit_fmt.setGeometry(QtCore.QRect(10, 305, 780, 285))
		self.textEdit_fmt.setObjectName("textEdit_raw")
		self.textEdit_fmt.setTabStopWidth(20)
		
		# language type
		self.comboBox_langMode = QtGui.QComboBox(CodeFormatterClass)
		self.comboBox_langMode.setGeometry(QtCore.QRect(10, 600, 120, 25))
		self.comboBox_langMode.setObjectName("comboBox_langMode")
		self.comboBox_langMode.insertItem(0, 'Python')
		self.comboBox_langMode.insertItem(1, 'Other')
		
		# Tab Width label
		self.label_tabWidth = QtGui.QLabel(CodeFormatterClass)
		self.label_tabWidth.setGeometry(QtCore.QRect(140, 600, 70, 25))
		self.label_tabWidth.setObjectName("label_tabWidt")
		
		# Tab Width lineEdit
		self.lineEdit_tabWidth = QtGui.QLineEdit(CodeFormatterClass)
		self.lineEdit_tabWidth.setGeometry(QtCore.QRect(210, 600, 30, 25))
		self.lineEdit_tabWidth.setObjectName("lineEdit_tabWidth")
		self.lineEdit_tabWidth.setText('4')
		
		# 'Convert' button
		self.pushButton_convert = QtGui.QPushButton(CodeFormatterClass)
		self.pushButton_convert.setGeometry(QtCore.QRect(250, 600, 90, 25))
		self.pushButton_convert.setObjectName("pushButton_convert")
		
		# 'clear all' button
		self.pushButton_clear = QtGui.QPushButton(CodeFormatterClass)
		self.pushButton_clear.setGeometry(QtCore.QRect(350, 600, 90, 25))
		self.pushButton_clear.setObjectName("pushButton_clear")
		
		# 'Open' button
		self.pushButton_open = QtGui.QPushButton(CodeFormatterClass)
		self.pushButton_open.setGeometry(QtCore.QRect(450, 600, 90, 25))
		self.pushButton_open.setObjectName("pushButton_open")		
		
		# 'SaveAs' button
		self.pushButton_saveAs = QtGui.QPushButton(CodeFormatterClass)
		self.pushButton_saveAs.setGeometry(QtCore.QRect(550, 600, 90, 25))
		self.pushButton_saveAs.setObjectName("pushButton_saveAs")
		
		# 'Copy to clipboard' button
		self.pushButton_clipboard = QtGui.QPushButton(CodeFormatterClass)
		self.pushButton_clipboard.setGeometry(QtCore.QRect(650, 600, 140, 25))
		self.pushButton_clipboard.setObjectName("pushButton_clipboard")
		
		# Open fileDialog
		self.fileDialog_open = QtGui.QFileDialog(CodeFormatterClass)
		
		# SaveAs fileDialog
		self.fileDialog_saveAs = QtGui.QFileDialog(CodeFormatterClass)
		
		self.retranslateUi(CodeFormatterClass)
		QtCore.QObject.connect(self.pushButton_convert, QtCore.SIGNAL("clicked()"), CodeFormatterClass.onConvertClicked)
		QtCore.QObject.connect(self.pushButton_clear, QtCore.SIGNAL("clicked()"), CodeFormatterClass.onClearAllClicked)
		QtCore.QObject.connect(self.pushButton_open,QtCore.SIGNAL("clicked()"),CodeFormatterClass.onOpenClicked)		
		QtCore.QObject.connect(self.pushButton_saveAs, QtCore.SIGNAL("clicked()"), CodeFormatterClass.onSaveAsClicked)
		QtCore.QObject.connect(self.pushButton_clipboard, QtCore.SIGNAL("clicked()"), CodeFormatterClass.onCpClipboardClicked)		
		QtCore.QObject.connect(self.lineEdit_tabWidth,QtCore.SIGNAL("returnPressed()"),CodeFormatterClass.onTabWidthSeted)		
		QtCore.QMetaObject.connectSlotsByName(CodeFormatterClass)

	def retranslateUi(self, CodeFormatterClass):
		CodeFormatterClass.setWindowTitle(QtGui.QApplication.translate("CodeFormatterClass", "Code Formatter", None, QtGui.QApplication.UnicodeUTF8))
		self.pushButton_convert.setText(QtGui.QApplication.translate("CodeFormatterClass", "Convert", None, QtGui.QApplication.UnicodeUTF8))
		self.pushButton_clipboard.setText(QtGui.QApplication.translate("CodeFormatterClass", "Copy to Clipboard", None, QtGui.QApplication.UnicodeUTF8))		
		self.pushButton_saveAs.setText(QtGui.QApplication.translate("CodeFormatterClass", "Save As...", None, QtGui.QApplication.UnicodeUTF8))		
		self.pushButton_clear.setText(QtGui.QApplication.translate("CodeFormatterClass", "Clear All", None, QtGui.QApplication.UnicodeUTF8))				
		self.label_tabWidth.setText(QtGui.QApplication.translate("CodeFormatterClass", "Tab width:", None, QtGui.QApplication.UnicodeUTF8))
		self.pushButton_open.setText(QtGui.QApplication.translate("CodeFormatterClass", "Open...", None, QtGui.QApplication.UnicodeUTF8))

#================= Ui_CodeFormatterClass Start ===================