#! /usr/bin/python

################################################################
# Project: Python Code Formatter
# Filename: CodeFormatter.py
# Description: An useful tool for source code formatting
# Date: 2009.05.24
# Programmer: Lin Xin Yu
# E-mail: nxforce@yahoo.com
# Website:http://importcode.blogspot.com
################################################################

#================= import modules ==============================
import re
import sys
import ui_codeformatter
from PyQt4 import QtCore, QtGui

#================= CodeFormatter Class Start ===================

class CodeFormatter(QtGui.QMainWindow):
	def __init__(self, parent = None):
		QtGui.QMainWindow.__init__(self, parent)
		self.ui = ui_codeformatter.Ui_CodeFormatterClass()
		self.ui.setupUi(self)
		
		self.ui.textEdit_fmt.setEnabled(False)
		
		# default tab width is 4 for all language 
		self.tabWidth = 4
		
		# init raw text
		self.rawText = ''		
		
		# init formated text
		self.fmtText = ''

	# other code phrasing
	def otherCodePhrasing(self):
		# add the start css tag of google code prettify 
		self.fmtText = '<code class="prettyprint">'
		
		for line in self.rawText:
			self.fmtText += line.replace('<', '&lt;').replace('>','&gt;') + '\n'
		
		# add the end css tag of google code prettify 
		self.fmtText += '</code>'
		
		self.ui.textEdit_fmt.setPlainText(self.fmtText)
	
	# python source code phrasing
	def pythonCodePhrasing(self):
		numOfTabs = 0
		numOfSpaces = 0
		
		# list for recording the white spaces
		list = [0]
		
		# add the start css tag of google code prettify 
		self.fmtText = '<code class="prettyprint">'
		
		for line in self.rawText:
			numOfChrs = 0
			nSpaces = 0
			
			matchAnySpace = re.match('[\s]+', line)
			if(matchAnySpace):
				nSpaces = matchAnySpace.group().count('\t') * self.tabWidth				
				nSpaces += matchAnySpace.group().count(' ')
				numOfChrs += len(matchAnySpace.group())
			
			tmpLine = line[numOfChrs:].replace('<', '&lt;').replace('>','&gt;')			
			
			# if this line is newline only	
			if(tmpLine == ''):
				self.fmtText += '&nbsp;' * (numOfTabs * self.tabWidth) + '\n'
				continue
			
			### reserved function for worst case ###
			# if this line is commecnt
			#matchComment = re.match('^#', tmpLine)	
			#if(matchComment):
			#	self.fmtText += '&nbsp;' * (numOfTabs * self.tabWidth) + tmpLine + '\n'
			#	continue
			
			# update tab character count by verifying the white spaces
			if(nSpaces > numOfSpaces):
				list.append(nSpaces)
				numOfTabs += 1
			elif(nSpaces < numOfSpaces):
				while list[len(list)-1] != nSpaces:
					list.pop()
					numOfTabs -= 1
			
			numOfSpaces = nSpaces			
			
			# when the line is a new class or function, reset all counts
			if(numOfChrs == 0):
				numOfSpaces = numOfTabs = 0			
				
			self.fmtText += '&nbsp;' * (numOfTabs * self.tabWidth) + tmpLine + '\n'
		
		# add the end css tag of google code prettify 
		self.fmtText += '</code>'
		
		self.ui.textEdit_fmt.setPlainText(self.fmtText)		
		
	# SLOT for convert button
	def onConvertClicked(self):
		self.ui.textEdit_fmt.setEnabled(True)
		self.rawText = str(self.ui.textEdit_raw.toPlainText()).split('\n')
		
		if(self.ui.comboBox_langMode.currentIndex() == 0):
			self.pythonCodePhrasing()
			return True
			
		if(self.ui.comboBox_langMode.currentIndex() == 1):
			self.otherCodePhrasing()
			return True
			
	def onClearAllClicked(self):
	  self.ui.textEdit_raw.setText('')
	  self.ui.textEdit_fmt.setText('')
	  self.ui.textEdit_raw.setFocus()
	
	def onCpClipboardClicked(self):
		self.ui.textEdit_fmt.selectAll()
		self.ui.textEdit_fmt.copy()
		mimeData = QtGui.QApplication.clipboard().mimeData();
	
	def onOpenClicked(self):
		filename = self.ui.fileDialog_open.getOpenFileName(self, 'Open file', QtCore.QDir.homePath()+'/Desktop')
		if(filename):
			fileIn = open(filename, 'r')
			rawText = fileIn.read()
			fileIn.close()
			self.ui.textEdit_raw.setPlainText(rawText)		
	
	def onSaveAsClicked(self):
		filename = self.ui.fileDialog_saveAs.getSaveFileName(self, 'Save file', QtCore.QDir.homePath()+'/Desktop')
		if(filename):
			fileOut = open(filename, 'w')
			fileOut.write(self.fmtText)
			fileOut.close()
	
	def onTabWidthSeted(self):
		self.tabWidth = int(self.ui.lineEdit_tabWidth.text())
	

#================= CodeFormatter Class End ===================

if __name__ == '__main__':
	app = QtGui.QApplication(sys.argv)
	codeFormatter = CodeFormatter()
	codeFormatter.show()
	sys.exit(app.exec_())